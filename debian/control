Source: python-certbot-dns-sakuracloud
Maintainer: Debian Let's Encrypt Team <team+letsencrypt@tracker.debian.org>
Uploaders: Harlan Lieberman-Berg <hlieberman@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               python3-acme-abi-2 (>= 2.1~),
               python3-certbot-abi-2 (>= 2.1),
               python3-lexicon (>= 3.14.1),
               python3-mock,
               python3-pytest,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               python3-zope.interface
Standards-Version: 4.6.2
Homepage: https://certbot.eff.org/
Vcs-Git: https://salsa.debian.org/letsencrypt-team/certbot/certbot-dns-sakuracloud.git
Vcs-Browser: https://salsa.debian.org/letsencrypt-team/certbot/certbot-dns-sakuracloud
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-certbot-dns-sakuracloud
Architecture: all
Depends: certbot,
         python3-certbot-abi-2 (>= ${Abi-major-minor-version}),
         ${misc:Depends},
         ${python3:Depends}
Enhances: certbot
Description: SakuraCloud DNS plugin for Certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This agent is used to:
 .
   - Automatically prove to the Let's Encrypt CA that you control the website
   - Obtain a browser-trusted certificate and set it up on your web server
   - Keep track of when your certificate is going to expire, and renew it
   - Help you revoke the certificate if that ever becomes necessary.
 .
 This package contains the SakuraCloud DNS plugin to the main application.

Package: python-certbot-dns-sakuracloud-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Documentation for the SakuraCloud DNS plugin for Certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This package contains the documentation for the SakuraCloud DNS plugin to
 the main application.
